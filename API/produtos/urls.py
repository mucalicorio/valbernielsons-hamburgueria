from django.urls import path
from produtos import views


urlpatterns = [
    path('produtos/', views.ProdutosList.as_view()),
    path('produtos/<int:pk>/', views.ProdutosDetails.as_view()),
]
