from django.db import models


class Produtos(models.Model):
    categoria = models.CharField(max_length=100, blank=False)
    nome = models.CharField(max_length=100, blank=False)
    precoCusto = models.FloatField(blank=False)
    precoVenda = models.FloatField(blank=False)
    unidadeMedida = models.CharField(max_length=50, blank=False)
    descricao = models.CharField(max_length=200, blank=False)
