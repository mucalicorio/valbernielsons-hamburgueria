from rest_framework import serializers
from produtos.models import Produtos


class ProdutosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Produtos
        fields = '__all__'
