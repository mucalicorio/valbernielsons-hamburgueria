from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies =[
    ]

    operations = [
        migrations.CreateModel(
            name='Produtos',
            fields=[
                ('categoria', models.CharField(max_length=100, blank=False)),
                ('nome', models.CharField(max_length=100, blank=False)),
                ('precoCusto', models.FloatField(blank=False)),
                ('precoVenda', models.FloatField(blank=False)),
                ('unidadeMedida', models.CharField(max_length=50, blank=50)),
                ('descricao', models.CharField(max_length=200, blank=False)),
            ]
        )
    ]
