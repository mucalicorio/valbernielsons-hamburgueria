from produtos.models import Produtos
from produtos.serializers import ProdutosSerializer

from rest_framework import generics


class ProdutosList(generics.ListCreateAPIView):
    queryset = Produtos.objects.all()
    serializer_class = ProdutosSerializer


class ProdutosDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Produtos.objects.all()
    serializer_class = ProdutosSerializer
