from django.urls import path
from login import views


urlpatterns = [
    path('login/', views.LoginList.as_view()),
    path('login/<int:pk>/', views.LoginDetails.as_view()),
]
