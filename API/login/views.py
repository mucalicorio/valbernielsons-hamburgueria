from login.models import Login
from login.serializers import LoginSerializer

from rest_framework import generics


class LoginList(generics.ListCreateAPIView):
    queryset = Login.objects.all()
    serializer_class = LoginSerializer


class LoginDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Login.objects.all()
    serializer_class = LoginSerializer
