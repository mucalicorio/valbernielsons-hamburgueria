from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Login',
            fields=[
                ('nome', models.CharField(max_length=100, blank=False)),
                ('usuario', models.CharField(max_length=100, blank=False)),
                ('senha', models.CharField(max_length=100, blank=False)),
            ]
        )
    ]
