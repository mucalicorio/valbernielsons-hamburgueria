from django.db import models
from datetime import datetime


class Pedidos(models.Model):
    nomeUsuario = models.CharField(max_length=100, blank=False)
    horarioAbertura = models.DateTimeField(default=datetime.now, blank=True)
    status = models.CharField(max_length=50, blank=False)
    produtos = models.CharField(max_length=500, blank=False)
    total = models.IntegerField(blank=False)
