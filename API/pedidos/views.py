from pedidos.models import Pedidos
from pedidos.serializers import PedidosSerializer

from rest_framework import generics


class PedidosList(generics.ListCreateAPIView):
    queryset = Pedidos.objects.all()
    serializer_class = PedidosSerializer


class PedidosDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Pedidos.objects.all()
    serializer_class = PedidosSerializer
