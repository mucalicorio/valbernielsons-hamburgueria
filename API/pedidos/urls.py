from django.urls import path
from pedidos import views


urlpatterns = [
    path('pedidos/', views.PedidosList.as_view()),
    path('pedidos/<int:pk>/', views.PedidosDetails.as_view()),
]
