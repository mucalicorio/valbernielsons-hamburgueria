from django.db import migrations, models
from datetime import datetime


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pedidos',
            fields=[
                ('nomeUsuario', models.CharField(max_length=100, blank=False)),
                ('horarioAbertura', models.DateTimeField(default=datetime.now, blank=True)),
                ('status', models.CharField(max_length=50, blank=False)),
                ('produtos', models.CharField(max_length=500, blank=False)),
                ('total', models.IntegerField(blank=False)),
            ]
        )
    ]
