# API
A API foi desenvolvida em Python 3.7 utilizando o Django Rest Framework. Como banco de dados utilizei o PostgreSQL (versão 11). Dentro da pasta existe um arquivo requirements.txt (ele roda tanto os requerimentos para produção quanto o ambiente para desenvolvimento, que é o arquivo requirements-dev.txt).
Para rodar a API (com a configuração do APLICATIVO) basta rodar os seguintes comandos:
* `python manage.py migrate`
* `python manage.py runserver 0.0.0.0:5000`

## Configuração do Banco de Dados

* database: hamburgueria
* username: postgres
* password: admin
* host: localhost
* port: 5432

## Cadastro de Produtos
Os produtos só podem ser cadastrados pela API. Entre em [PRODUTOS](http://localhost:5000/produtos/) e faça os cadastros manualmente.

# DB
Fiz um backup do banco de dados. Está dentro da pasta [DB](https://gitlab.com/mucalicorio/valbernielsons-hamburgueria/tree/master/DB).

# APLICATIVO
O APLICATIVO foi desenvolvido em TypeScript e HTML (utilizando o Ionic Framework (versão 3.2.1)). É um Aplicativo Mobile, então fica bem melhor pra visualizar em um celular ou no modo de Inspeção de Elementos (com a responsividade de um celular) nos navegadores.
Para rodar o aplicativo, basta rodar os seguintes comandos:
* `npm i`
* `ionic serve`

Obs: Se estiver logado com o usuário **admin**, todos os pedidos poderão ser visualizados e alterados. Outra pequena observação, é que as telas possuem refresh (arrastar pra baixo para atualizar).