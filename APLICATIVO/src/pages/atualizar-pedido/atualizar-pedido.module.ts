import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AtualizarPedidoPage } from './atualizar-pedido';

@NgModule({
  declarations: [
    AtualizarPedidoPage,
  ],
  imports: [
    IonicPageModule.forChild(AtualizarPedidoPage),
  ],
})
export class AtualizarPedidoPageModule {}
