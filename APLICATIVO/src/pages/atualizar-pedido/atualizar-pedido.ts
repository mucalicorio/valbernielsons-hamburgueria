import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-atualizar-pedido',
  templateUrl: 'atualizar-pedido.html',
})
export class AtualizarPedidoPage {

  public id = 'null';
  public nomeUsuario = 'null';
  public status = 'null';
  public produtos = 'null';
  public total = 'null';

  public formAtualizarPedido: FormGroup;
  public totalFormulario: number = 17;

  dataPost: any = {};

  public produtosBanco: any = [];
  public precoVendaProduto: any = 0;

  constructor(public formBuilder: FormBuilder, public httpClient: HttpClient, public navCtrl: NavController, public navParams: NavParams) {
    this.id = navParams.get('id');
    this.nomeUsuario = navParams.get('nomeUsuario');
    this.status = navParams.get('status');
    this.produtos = navParams.get('produtos');
    this.total = navParams.get('total');

    this.getProdutos();

    console.log('ID Atualizar Pedido Page: ', this.id);

    this.formAtualizarPedido = this.formBuilder.group({
      produtos: ['', Validators.required],
      quantidade: ['', Validators.required]
    });

  }

  patchData(dataPost) {
    this.httpClient.patch(`http://localhost:5000/pedidos/${this.id}/`, JSON.stringify(dataPost), {
      headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8')
    })
      .subscribe((result => {
        console.log('Result: ', result)
      }))
  }


  confirmarAtualizacao() {
    let produtosPatch = this.produtos + ', ' + this.formAtualizarPedido.controls.quantidade.value + ' ' + this.formAtualizarPedido.controls.produtos.value;
    console.log('Produtos PATCH: ', produtosPatch);

    let valor1 = parseFloat(this.formAtualizarPedido.controls.quantidade.value);

    for (var contador = 0; contador < this.produtosBanco.length; contador++) {
      let produtoBanco = this.produtosBanco[contador].nome;
      console.log('Produto banco: ', produtoBanco);
      if (this.formAtualizarPedido.controls.produtos.value == produtoBanco) {
        this.precoVendaProduto = this.produtosBanco[contador].precoVenda;
        console.log('Preço Venda: ', this.precoVendaProduto);
      }
    }

    let valor2 = parseFloat(this.precoVendaProduto);

    console.log('Tipo Preço venda Prodto 2: ', typeof (this.precoVendaProduto), ' = ', this.precoVendaProduto);

    this.totalFormulario = valor1 * valor2;
    console.log('Total Formulário: ', this.totalFormulario);

    let totalPatch = this.total + this.totalFormulario;
    console.log('Total Patch: ', totalPatch);

    this.dataPost = {
      "produtos": produtosPatch,
      "total": totalPatch
    }

    this.patchData(this.dataPost);
    
    this.navCtrl.popToRoot();
  }


  cancelarPedido() {
    this.dataPost = {
      "status": "Cancelado"
    }

    this.patchData(this.dataPost);

    this.navCtrl.popToRoot();
  }


  getProdutos() {
    this.httpClient.get('http://localhost:5000/produtos/')
      .subscribe((result) => {
        this.produtosBanco = result;
      })
  }

}
