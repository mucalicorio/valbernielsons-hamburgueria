import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AtualizarPedidoPage } from '../atualizar-pedido/atualizar-pedido';

import { HttpClient } from '@angular/common/http';


@IonicPage()
@Component({
  selector: 'page-detalhes-pedido',
  templateUrl: 'detalhes-pedido.html',
})
export class DetalhesPedidoPage {

  public id = 'null';

  pedido: any = {
    id: 'null',
    nomeUsuario: 'null',
    horarioAbertura: 'null',
    status: 'null',
    produtos: 'null',
    total: 'null'
  }

  data: any = 'null';
  hora: any = 'null';

  public statusOk: boolean;

  constructor(public httpClient: HttpClient, public navCtrl: NavController, public navParams: NavParams) {
    this.id = navParams.get('id');

    console.log('ID: ', this.id);

    this.getPedido(this.id);
  }


  atualizarButton() {
    console.log('Atualizar');

    this.navCtrl.push(AtualizarPedidoPage, {
      id: this.id,
      nomeUsuario: this.pedido.nomeUsuario,
      status: this.pedido.status,
      produtos: this.pedido.produtos,
      total: this.pedido.total
    })
  }


  getPedido(id) {
    this.statusOk = false;
    return this.httpClient.get(`http://localhost:5000/pedidos/${id}`)
      .subscribe((result => {
        this.pedido = result;

        console.log('Pedido: ', this.pedido);

        this.pedido.horarioAbertura = this.pedido.horarioAbertura.split('T');
        this.data = this.pedido.horarioAbertura[0];

        this.pedido.horarioAbertura = this.pedido.horarioAbertura[1].split('.');
        this.hora = this.pedido.horarioAbertura[0];

        console.log('Status Ok: ', this.statusOk);

        if (this.pedido.status !== "Finalizado" && this.pedido.status !== "Preparando" && this.pedido.status !== "Cancelado") {
          this.statusOk = true;

          console.log('Status Ok: ', this.statusOk);
          console.log('Status: ', this.pedido.status);
        }

        console.log('Data: ', this.data);
        console.log('Hora: ', this.hora);
      }))
  }

}
