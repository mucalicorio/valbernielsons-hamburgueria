import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarrinhoComprasPage } from './carrinho-compras';

@NgModule({
  declarations: [
    CarrinhoComprasPage,
  ],
  imports: [
    IonicPageModule.forChild(CarrinhoComprasPage),
  ],
})
export class CarrinhoComprasPageModule {}
