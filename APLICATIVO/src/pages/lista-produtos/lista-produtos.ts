import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { DetalhesProdutoPage } from '../detalhes-produto/detalhes-produto';


@IonicPage()
@Component({
  selector: 'page-lista-produtos',
  templateUrl: 'lista-produtos.html',
})
export class ListaProdutosPage {

  public produtos: any;

  public contadorAuxiliar: any;

  column: string = 'nome';
  ascending: boolean = false;
  order: number;

  constructor(public httpClient: HttpClient, public navCtrl: NavController, public navParams: NavParams) {
  this.getProdutos();
  }


  async getProdutos() {
    this.contadorAuxiliar = [];
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(
          this.httpClient.get('http://localhost:5000/produtos/')
            .subscribe((result => {
              this.produtos = result;

              for (var item = 0; item < this.produtos.length; item++) {
                this.contadorAuxiliar.push(item);
              }

              console.log('Produtos: ', this.produtos);
              console.log('Contador Auxiliar: ', this.contadorAuxiliar);
              console.log('Nome: ', this.produtos[this.contadorAuxiliar[1]].nome)
            }))
        );
      }, 100);
    })
  }


  getDetalhesProduto(id: number) {
    this.navCtrl.push(DetalhesProdutoPage, {
      id: id
    })
  }


  sort() {
    console.log('Sort');
    console.log('Columns LISTA ID: ', this.column);
    this.ascending = !this.ascending;
    this.order = this.ascending ? 1 : -1;
  }


  doRefresh(refresher) {
    this.getProdutos();

    setTimeout(() => {
      refresher.complete();
    }, 1500)
  }

}
