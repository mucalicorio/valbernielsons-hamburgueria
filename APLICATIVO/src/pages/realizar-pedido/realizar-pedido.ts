import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@IonicPage()
@Component({
  selector: 'page-realizar-pedido',
  templateUrl: 'realizar-pedido.html',
})
export class RealizarPedidoPage {

  usuario: any = 'null';

  pedido: any = '';

  formRealizarPedido: FormGroup;

  public produtosBanco: any = [];
  public precoVendaProduto: any = 0;
  public totalFormulario: any = 0;
  public totalPedido: any = 0;

  public dataPost: any;

  constructor(public httpClient: HttpClient, public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.usuario = this.navParams.get('usuario');

    this.formRealizarPedido = this.formBuilder.group({
      produtos: ['', Validators.required],
      quantidade: ['', Validators.required]
    });

    this.getProdutos();
  }


  adicionarLista() {
    if (this.pedido == '') {
      this.pedido = this.formRealizarPedido.controls.quantidade.value + ' ' + this.formRealizarPedido.controls.produtos.value;
    }
    else {
      this.pedido = this.pedido + ' | ' + this.formRealizarPedido.controls.quantidade.value + ' ' + this.formRealizarPedido.controls.produtos.value;
    }

    for (var contador = 0; contador < this.produtosBanco.length; contador++) {
      let produtoBanco = this.produtosBanco[contador].nome;
      console.log('Produto banco: ', produtoBanco);
      if (this.formRealizarPedido.controls.produtos.value == produtoBanco) {
        this.precoVendaProduto = this.produtosBanco[contador].precoVenda;
        console.log('Preço Venda: ', this.precoVendaProduto);
      }
    }

    this.totalFormulario = this.formRealizarPedido.controls.quantidade.value * this.precoVendaProduto;
    console.log('Total Formulário: ', this.totalFormulario);

    this.totalPedido = this.totalPedido + this.totalFormulario;
    console.log('Total Pedido: ', this.totalPedido);
  }


  confirmarRealizacaoPedido() {
    this.dataPost = {
      "nomeUsuario": this.usuario,
      "status": 'Aberto',
      "produtos": this.pedido,
      "total": this.totalPedido
    }

    this.postPedido(this.dataPost);

    this.navCtrl.popToRoot();
  }


  postPedido(dataPost) {
    this.httpClient.post('http://localhost:5000/pedidos/', JSON.stringify(dataPost), {
      headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8')
    })
      .subscribe((result => {
        console.log('Result: ', result)
      }))
  }


  getProdutos() {
    this.httpClient.get('http://localhost:5000/produtos/')
      .subscribe((result) => {
        this.produtosBanco = result;
      })
  }

}
