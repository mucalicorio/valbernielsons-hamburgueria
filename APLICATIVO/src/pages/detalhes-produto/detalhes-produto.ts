import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';


@IonicPage()
@Component({
  selector: 'page-detalhes-produto',
  templateUrl: 'detalhes-produto.html',
})
export class DetalhesProdutoPage {

  public id: any = 'null';

  produto: any = {
    id: 'null',
    categoria: 'null',
    nome: 'null',
    precoCusto: 'null',
    precoVenda: 'null',
    unidadeMedida: 'null',
    descricao: 'null'
  }

  constructor(public httpClient: HttpClient, public navCtrl: NavController, public navParams: NavParams) {
  this.id = navParams.get('id');

  console.log('ID Detalhes Produto: ', this.id);

  this.getProduto(this.id);
  }
  

  getProduto(id) {
    return this.httpClient.get(`http://localhost:5000/produtos/${id}`)
      .subscribe((result => {
        this.produto = result;
        this.produto.precoVenda = 'R$ ' + this.produto.precoVenda;
      }))
  }

}
