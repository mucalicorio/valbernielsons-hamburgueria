import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DetalhesPedidoPage } from '../detalhes-pedido/detalhes-pedido';
import { RealizarPedidoPage } from '../realizar-pedido/realizar-pedido';
import { ListaProdutosPage } from '../lista-produtos/lista-produtos';

import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public usuario = 'null';

  public pedidosUsuario: any = [];

  id: any = [];
  nomeUsuario: any = [];
  horarioAbertura: any = [];

  data: any = [];
  hora: any = [];

  status: any = [];
  produtos: any = [];
  total: any = [];

  contadorAuxiliar: any = [];

  ascending: boolean = false;
  order: number;
  column: string = 'horarioAbertura';

  constructor(public httpClient: HttpClient, public navCtrl: NavController, public navParams: NavParams) {
    this.usuario = navParams.get('usuario');
    this.getPedidosUsuario();

    console.log('Usuário: ', this.usuario);
  }


  async getPedidosUsuario() {
    this.pedidosUsuario = [];

    this.id = [];
    this.nomeUsuario = [];
    this.horarioAbertura = [];

    this.data = [];
    this.hora = [];

    this.status = [];
    this.produtos = [];
    this.total = [];

    this.contadorAuxiliar = [];

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(
          this.httpClient.get('http://localhost:5000/pedidos/')
            .subscribe((result => {
              this.pedidosUsuario = result;

              for (let item = 0; item < this.pedidosUsuario.length; item++) {
                this.id.push(this.pedidosUsuario[item].id);
                this.nomeUsuario.push(this.pedidosUsuario[item].nomeUsuario);
                this.horarioAbertura.push(this.pedidosUsuario[item].horarioAbertura);

                this.horarioAbertura[item] = this.horarioAbertura[item].split('T');
                this.data.push(this.horarioAbertura[item][0]);

                this.horarioAbertura[item] = this.horarioAbertura[item][1].split('.');
                this.hora.push(this.horarioAbertura[item][0]);

                this.status.push(this.pedidosUsuario[item].status);
                this.produtos.push(this.pedidosUsuario[item].produtos);
                this.total.push(this.pedidosUsuario[item].total);

                this.contadorAuxiliar.push(item);
              }

              console.log('Lista Pedidos: ', this.pedidosUsuario);
              console.log('Usuário: ', this.usuario);
              console.log('Id: ', this.id);
              console.log('Nome Usuário: ', typeof (this.nomeUsuario[0]));
              console.log('Horário Abertura: ', this.horarioAbertura);

              console.log('Data: ', this.data);
              console.log('Hora: ', this.hora);

              console.log('Status: ', this.status);
              console.log('Produtos: ', this.produtos);
              console.log('Total: ', this.total);
              console.log('Contador Auxiliar: ', this.contadorAuxiliar);
            }))
        );
      }, 100);
    })
  }


  listaProdutos() {
    this.navCtrl.push(ListaProdutosPage);
  }


  novoPedido() {
    this.navCtrl.push(RealizarPedidoPage, {
      usuario: this.usuario
    })
  }


  getDetalhesPedido(id: number) {
    this.navCtrl.push(DetalhesPedidoPage, {
      id: id
    })
  }


  sort() {
    console.log('Sort');
    console.log('Columns LISTA ID: ', this.column);
    this.ascending = !this.ascending;
    this.order = this.ascending ? 1 : -1;
  }


  doRefresh(refresher) {
    this.getPedidosUsuario();

    setTimeout(() => {
      refresher.complete();
    }, 1500)
  }

}
