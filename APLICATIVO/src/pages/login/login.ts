import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';
import { CadastrarUsuarioPage } from '../cadastrar-usuario/cadastrar-usuario';

import { HttpClient } from '@angular/common/http';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public usuariosBanco: any = [];

  public formLogin: FormGroup;

  constructor(public httpClient: HttpClient, public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.getUsuarios().then();

    this.formLogin = this.formBuilder.group({
      usuario: ['', Validators.required],
      senha: ['', Validators.required]
    });

  }


  confirmarLogin() {
    for (var contador = 0; contador < this.usuariosBanco.length; contador++) {
      let usuarioBanco = this.usuariosBanco[contador].usuario;
      if (this.formLogin.value.usuario == usuarioBanco) {
        let senhaBanco = this.usuariosBanco[contador].senha;
        if (this.formLogin.value.senha == senhaBanco) {
          console.log('Login feito com sucesso.');

          this.navCtrl.setRoot(HomePage, {
            id: this.formLogin.value.id,
            nome: this.formLogin.value.nome,
            usuario: this.formLogin.value.usuario
          });
        }

        else {
          console.log('Senha incorreta.');
        }
      }

      else if (contador + 1 == this.usuariosBanco.length) {
        console.log('Usuário Inexistente.');
      }
    }
  }


  cadastrarUsuario() {
    this.navCtrl.push(CadastrarUsuarioPage);
  }

  
  async getUsuarios() {
    this.usuariosBanco = [];

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(
          this.httpClient.get('http://localhost:5000/login/')
            .subscribe((result => {
              this.usuariosBanco = result;
              console.log('Lista Itens: ', this.usuariosBanco)
            }))
        );
      }, 100);
    })
  }


  doRefresh(refresher) {
    this.getUsuarios();

    setTimeout(() => {
      refresher.complete();
    }, 1500)
  }

}
