import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-cadastrar-usuario',
  templateUrl: 'cadastrar-usuario.html',
})
export class CadastrarUsuarioPage {

  public usuariosBanco: any = [];
  public usuarioExistente: boolean = false;

  public formCadastrarUsuario: FormGroup;

  constructor(public httpClient: HttpClient, public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
    this.getUsuarios().then();

    this.formCadastrarUsuario = this.formBuilder.group({
      nome: ['', Validators.required],
      usuario: ['', Validators.required],
      senha: ['', Validators.required],
      confirmarSenha: ['', Validators.required]
    }, { validator: this.conferirSenhas('senha', 'confirmarSenha') });

  }


  conferirSenhas(senhaKey: string, confirmarSenhaKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let senha = group.controls[senhaKey];
      let confirmarSenha = group.controls[confirmarSenhaKey];

      if (senha.value !== confirmarSenha.value) {
        return {
          senhasDiferentes: true
        };
      }
    }
  }


  cadastrarUsuario() {
    for (var contador = 0; contador < this.usuariosBanco.length; contador++) {
      let usuarioBanco = this.usuariosBanco[contador].usuario;
      if (this.formCadastrarUsuario.value.usuario == usuarioBanco) {
        this.usuarioExistente = true;
      }
    }

    if (this.usuarioExistente === false) {
      console.log('USUÁRIO CADASTRADO COM SUCESSO! Usuário: ', this.formCadastrarUsuario.value.usuario);

      let dadosPost = {
        "nome": this.formCadastrarUsuario.value.nome,
        "usuario": this.formCadastrarUsuario.value.usuario,
        "senha": this.formCadastrarUsuario.value.senha
      };

      this.postUsuario(dadosPost);

      this.navCtrl.popToRoot();
    }

    else {
      console.log('Este usuário já está cadastrado.');
      this.usuarioExistente = false;
    }
  }


  cancelarCadastro() {
    this.navCtrl.popToRoot();
  }


  async getUsuarios() {
    this.usuariosBanco = [];

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(
          this.httpClient.get('http://localhost:5000/login/')
            .subscribe((result => {
              this.usuariosBanco = result;
              console.log('Lista Itens: ', this.usuariosBanco);
            }))
        );
      }, 100);
    })
  }


  postUsuario(dadosPost) {
    return this.httpClient.post('http://localhost:5000/login/', JSON.stringify(dadosPost), {
      headers: new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8')
    })
      .subscribe((result => {
        console.log('Result of Post: ', result);
      }))
  }

}
