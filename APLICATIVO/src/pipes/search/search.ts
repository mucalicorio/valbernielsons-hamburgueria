import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {

  transform(itens: any[], terms: string): any[] {
    if (!itens) return [];
    if (!terms) return itens;
    terms = terms.toLowerCase();
    return itens.filter(it => {
      let text = Object.keys(it)
        .map(key => {
          return it[key]
        })
        .join( " ");
      console.log('Text: ', text);
      return text.toLowerCase().includes(terms);
    })
  }
}
