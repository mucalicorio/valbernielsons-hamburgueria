import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'sort',
})
export class SortPipe implements PipeTransform {

  transform(lista: Array<string>, args?: any): Array<string> {
    return lista.sort(function (a, b) {
      if (a[args.property] < b[args.property]) {
        return -1 * args.order;
      }
      else if (a[args.property] > b[args.property]) {
        return 1 * args.order;
      }
      else {
        return 0;
      }
    });
  }
}
