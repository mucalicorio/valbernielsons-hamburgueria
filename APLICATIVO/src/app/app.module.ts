import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { CadastrarUsuarioPage } from '../pages/cadastrar-usuario/cadastrar-usuario';
import { AtualizarPedidoPage } from '../pages/atualizar-pedido/atualizar-pedido';
import { ListaProdutosPage } from '../pages/lista-produtos/lista-produtos';
import { RealizarPedidoPage } from '../pages/realizar-pedido/realizar-pedido';

import { HttpClientModule } from '@angular/common/http';
import { SearchPipe } from '../pipes/search/search';
import { SortPipe } from '../pipes/sort/sort';
import { DetalhesPedidoPage } from '../pages/detalhes-pedido/detalhes-pedido';
import { DetalhesProdutoPage } from '../pages/detalhes-produto/detalhes-produto';

@NgModule({
  declarations: [
    MyApp,

    AtualizarPedidoPage,
    CadastrarUsuarioPage,
    DetalhesPedidoPage,
    DetalhesProdutoPage,
    HomePage,
    ListaProdutosPage,
    LoginPage,
    RealizarPedidoPage,

    SearchPipe,
    SortPipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),

    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AtualizarPedidoPage,
    CadastrarUsuarioPage,
    DetalhesPedidoPage,
    DetalhesProdutoPage,
    HomePage,
    ListaProdutosPage,
    LoginPage,
    RealizarPedidoPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},

    HttpClientModule
  ]
})
export class AppModule {}
